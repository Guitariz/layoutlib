package layout
{
	public class Layouts
	{
		//-----------------------------------------------------------
		//
		// Constants
		//
		//-----------------------------------------------------------
		
		public static const RELATIVE			: String = "RELATIVE";
		public static const HORIZONTAL			: String = "HORIZONTAL";
		public static const VERTICAL			: String = "VERTICAL";
		
	}
}