package layout
{
	import flash.geom.Point;
	
	import align.interfaces.IBaseContainer;
	
	import factory.IcomponentFactory;

	public class ConstructMethods
	{
		//-----------------------------------------------------------
		//
		// Constants
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Constructor
		//
		//-----------------------------------------------------------
		
		public function ConstructMethods()
		{
		}
		
		//-----------------------------------------------------------
		//
		// Variables
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Overriden properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Overriden methods
		//
		//----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Methods
		//
		//-----------------------------------------------------------
		
		public static function assignSettings(src:IBaseContainer):void
		{
			if (src.settings.hasOwnProperty('size') )
				src.contentSize = new Point(src.settings['size'][0], src.settings['size'][1]);
			
			if (src.settings.hasOwnProperty('layout'))
				src.layout = src.settings['layout'];
			
			if (src.settings.hasOwnProperty('align'))
				src.align.adds( src.settings['align']);
		}
		
		//-----------------------------------------------------------
		//
		// Handlers
		//
		//-----------------------------------------------------------
	}
}