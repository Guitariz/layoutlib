package layout
{
	import flash.geom.Point;
	
	import align.interfaces.IBaseContainer;

	public class LayoutMethods
	{
		//---------------------------------------------------------------
		//
		// Variables
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Visual
		//
		//---------------------------------------------------------------
		
		public static function arrangeChildsFromRelative(src:IBaseContainer):Point
		{
			var maxSize:Point = calcMaxChildrenSize(src);
			
			var child:IBaseContainer;
			for (var i:int = 0; i < src.childrens.length; i++) 
			{
				child = src.childrens[i];
				arrangeChildToH(child, maxSize);
				arrangeChildToW(child, maxSize);
			}
			
			return maxSize;
		}
		
		public static function arrangeChildsVertical(src:IBaseContainer):Point
		{
			var maxSize:Point = calcMaxChildrenSize(src);
			
			var child:IBaseContainer;
			
			var height:Number = 0;
			for (var i:int = 0; i < src.childrens.length; i++) 
			{
				child = src.childrens[i];
				if (!child.visible)
					continue;
				arrangeChildToW(child, maxSize);
				child.y = height;
				height += child.contentSize.y;
			}
			
			return new Point(maxSize.x, height);
		}
		
		public static function arrangeChildsHorizontal(src:IBaseContainer):Point
		{
			var maxSize:Point = calcMaxChildrenSize(src);
			
			var child:IBaseContainer;
			
			var width:Number = 0;
			for (var i:int = 0; i < src.childrens.length; i++) 
			{
				child = src.childrens[i];
				if (!child.visible)
					continue;
				arrangeChildToH(child, maxSize);
				child.x = width;
				width += child.contentSize.x;
			}
			
			return new Point(width, maxSize.y);
		}
		
		//---------------------------------------------------------------
		//
		// Logic
		//
		//---------------------------------------------------------------
		
		private static function calcMaxChildrenSize(src:IBaseContainer):Point
		{
			var result:Point = new Point(0,0);
			for (var i:int = 0; i < src.childrens.length; i++) 
			{
				src.childrens[i].arrangeChilds();
				if (!src.childrens[i].visible)
					continue;
				result.x = Math.max(result.x, src.childrens[i].contentSize.x);
				result.y = Math.max(result.y, src.childrens[i].contentSize.y);
			}
			
			if (src.align.isHStretch)
				result.x = Math.max(src.contentSize.x, result.x);
			
			if (src.align.isVStretch)
				result.y = Math.max(src.contentSize.y, result.y);
			
			return result;
		}
		
		private static function arrangeChildToH(child:IBaseContainer, maxSize:Point):void
		{
			switch(true)
			{
				case child.align.isLeft:
					child.x = 0;
					break;
				case child.align.isHCenter:
					child.x = (maxSize.x - child.contentSize.x) * 0.5;
					break;
				case child.align.isRight:
					child.x = maxSize.x - child.contentSize.x;
					break;
				case child.align.isHStretch :
					child.contentSize = maxSize;
					child.x = 0;
					break;
				case child.align.isHNone :
					child.x = child.pos.x;
			}
		}
		
		private static function arrangeChildToW(child:IBaseContainer, maxSize:Point):void
		{
			switch(true)
			{
				case child.align.isTop:
					child.y = 0;
					break;
				case child.align.isVCenter:
					child.y = (maxSize.y - child.contentSize.y) * 0.5;
					break;
				case child.align.isBottom:
					child.y = maxSize.y - child.contentSize.y;
					break;
				case child.align.isVStretch :
					child.contentSize = maxSize;
					child.y = 0;
					break;
				case child.align.isVNone :
					child.y = child.pos.y;
			}
		}
		
		
		//---------------------------------------------------------------
		//
		// Handlers
		//
		//---------------------------------------------------------------
	}
}