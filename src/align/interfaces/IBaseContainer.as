package align.interfaces
{
	import flash.geom.Point;
	
	import align.settings.ContAlign;

	public interface IBaseContainer
	{
		function get x():Number;
		function set x(value:Number):void;
		
		function get y():Number;
		function set y(value:Number):void;
		
		function get pos():Point;
		function set pos(value:Point):void;
	
		function get layout():String;
		function set layout(value:String):void;
		
		function get contentSize():Point;
		function set contentSize(value:Point):void;
		
		function get visible():Boolean;
		function set visible(value:Boolean):void;
		
		function get id():String;
		function set id(value:String):void;

		function get align():ContAlign;
		
		function get childrens():Vector.<IBaseContainer>;
		
		function addChildrenToLayout(child:IBaseContainer):IBaseContainer;
		function removeChildrenFromLayout(child:IBaseContainer):IBaseContainer;

		function arrangeChilds():void;
		
		function get display():*;
		
		
		//---------------------
		//
		//---------------------
		
		function createFromSettings(settings:*):void;
		function get settings():Object;
		
		function getChildWithID(id:String):*
	}
}