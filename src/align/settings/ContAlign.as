package align.settings
{
	public class ContAlign
	{
		//-----------------------------------------------------------
		//
		// Constants
		//
		//-----------------------------------------------------------
		
		public static const TOP				:String = "TOP";
		public static const VCENTER			:String = "VCENTER";
		public static const BOTTOM			:String = "BOTTOM";
		public static const VSTRETCH		:String = "VSTRETCH";
		public static const VNONE			:String = "VNONE";

		public static const LEFT			:String = "LEFT";
		public static const HCENTER			:String = "HCENTER";
		public static const RIGHT			:String = "RIGHT";
		public static const HSTRETCH		:String = "HSTRETCH";
		public static const HNONE			:String = "HNONE";
		

		
		
		
		//-----------------------------------------------------------
		//
		// Constructor
		//
		//-----------------------------------------------------------
		
		public function ContAlign(sets:Array)
		{
			if (sets != null)
				adds(sets);
		}
		
		public function add(value:String):void
		{
			switch (value)
			{
				case TOP:
					vAlign = TOP;
					break;
				case VCENTER:
					vAlign = VCENTER;
					break;
				case BOTTOM:
					vAlign = BOTTOM;
					break;
				case VSTRETCH:
					vAlign = VSTRETCH;
					break;
				
				
				
				case LEFT:
					hAlign = LEFT;
					break;
				case HCENTER:
					hAlign = HCENTER;
					break;
				case RIGHT:
					hAlign = RIGHT;
					break;
				case HSTRETCH:
					hAlign = HSTRETCH;
					break;
			}
		}
		
		public function adds(values:Array):void
		{
			for (var i:int = 0; i < values.length; i++) 
			{
				add(values[i]);
			}
			
		}
		
		//-----------------------------------------------------------
		//
		// Variables
		//
		//-----------------------------------------------------------
		
		private var vAlign:String = VNONE;
		private var hAlign:String = HNONE;
		
		//-----------------------------------------------------------
		//
		// Overriden properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Overriden methods
		//
		//----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Methods
		//
		//-----------------------------------------------------------
		
		public function get isLeft():Boolean
		{
			return hAlign == LEFT;
		}
		
		public function get isHCenter():Boolean
		{
			return hAlign == HCENTER;
		}
		
		public function get isRight():Boolean
		{
			return hAlign == RIGHT;
		}
		
		public function get isHStretch():Boolean
		{
			return hAlign == HSTRETCH;
		}
		
		public function get isHNone():Boolean
		{
			return hAlign == HNONE;
		}
		
		
		public function get isTop():Boolean
		{
			return vAlign == TOP;
		}
		
		public function get isVCenter():Boolean
		{
			return vAlign == VCENTER;
		}
		
		public function get isBottom():Boolean
		{
			return vAlign == BOTTOM;
		}
		
		public function get isVStretch():Boolean
		{
			return vAlign == VSTRETCH;
		}
		
		public function get isVNone():Boolean
		{
			return vAlign == VNONE;
		}
		
		//-----------------------------------------------------------
		//
		// Handlers
		//
		//-----------------------------------------------------------
	}
}