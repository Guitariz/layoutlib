package factory
{
	public interface IcomponentFactory
	{
		function createComponent(data:Object):*;
	}
}