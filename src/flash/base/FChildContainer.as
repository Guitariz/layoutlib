package flash.base
{
	import flash.geom.Point;
	
	import layout.LayoutMethods;
	import layout.Layouts;

	public class FChildContainer extends FBaseContainer
	{
		//-----------------------------------------------------------
		//
		// Constants
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Constructor
		//
		//-----------------------------------------------------------
		
		public function FChildContainer()
		{
			super();
			_layout = Layouts.RELATIVE;
		}
		
		//-----------------------------------------------------------
		//
		// Variables
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Overriden properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Overriden methods
		//
		//----------------------------------------------------------
		
		override public function arrangeChilds():void
		{
			var newSize:Point;
			switch (_layout)
			{
				case Layouts.RELATIVE:
					newSize = LayoutMethods.arrangeChildsFromRelative(this);
					break;
				
				case Layouts.HORIZONTAL:
					newSize = LayoutMethods.arrangeChildsHorizontal(this);
					break;
				
				case Layouts.VERTICAL:
					newSize = LayoutMethods.arrangeChildsVertical(this);
					break;
				
				default:
					trace("WRONG LAYOUT: " + _layout);
			}
			
			_contentSize = newSize;
		}
		
		override public function set contentSize(value:Point):void
		{
			if (value.equals(_contentSize))
				return;
			else
			{
				super.contentSize = value;
				arrangeChilds();
			}
		}	
	
		//-----------------------------------------------------------
		//
		// Methods
		//
		//-----------------------------------------------------------
		
		override public function getChildWithID(id:String):*
		{
			var child:* = super.getChildWithID(id);
			if(child)
				return child;
			else
				for (var i:int = 0; i < _childrens.length; i++) 
				{
					child = _childrens[i].getChildWithID(id);
					if(child)
						return child;
				}
			
			return null;

		}
		
		//-----------------------------------------------------------
		//
		// Handlers
		//
		//-----------------------------------------------------------

	
	}
}