package flash.base
{
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import align.interfaces.IBaseContainer;
	import align.settings.ContAlign;
	
	import layout.ConstructMethods;
	
	public class FBaseContainer extends Sprite implements IBaseContainer
	{
		//-----------------------------------------------------------
		//
		// Constants
		//
		//-----------------------------------------------------------
		
		protected static const NULLPOINT:Point = new Point();
		
		//-----------------------------------------------------------
		//
		// Constructor
		//
		//-----------------------------------------------------------
		
		public function FBaseContainer()
		{
			super();
		}
		
		//-----------------------------------------------------------
		//
		// Variables
		//
		//-----------------------------------------------------------
		
		protected var _contentSize:Point = new Point();
		
		protected var _align:ContAlign = new ContAlign( null );
		
		protected var _layout:String = "None";
		
		protected var _childrens:Vector.<IBaseContainer> = new Vector.<IBaseContainer>;
		
		protected var _pos:Point = new Point();
		
		protected var _id:String = null;
		
		//-----------------------------------------------------------
		//
		// Overriden properties
		//
		//-----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Properties
		//
		//-----------------------------------------------------------

		public function get id():String
		{
			return _id? _id: settings['id'];
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function get childrens():Vector.<IBaseContainer>
		{
			return _childrens;
		}

		public function get pos():Point
		{
			return _pos;
		}

		public function set pos(value:Point):void
		{
			_pos = value;
		}

		public function get layout():String
		{
			return _layout;
		}

		public function set layout(value:String):void
		{
			_layout = value;
		}

		public function get align():ContAlign
		{
			return _align;
		}

		public function get contentSize():Point
		{
			return _contentSize;
		}
		
		public function set contentSize(value:Point):void
		{
			_contentSize = value;
		}
		
		//-----------------------------------------------------------
		//
		// Overriden methods
		//
		//----------------------------------------------------------
		
		//-----------------------------------------------------------
		//
		// Methods
		//
		//-----------------------------------------------------------
		
		public function get settings():Object
		{
			return _settings;
		}
		
		protected var _settings:Object;
		
		
		public function createFromSettings(settings:*):void
		{
			this._settings = settings;

			assignMySettings();
		}
		
		protected function assignMySettings():void
		{
			ConstructMethods.assignSettings(this);
		}			
		
		
		public function addChildrenToLayout(child:IBaseContainer):IBaseContainer
		{
			_childrens.push(child);
			addChild(child.display);
			
			return child;
		}
		
		public  function removeChildrenFromLayout(child:IBaseContainer):IBaseContainer
		{
			removeChild(child.display);
			
			for (var i:int = 0; i < _childrens.length; i++) 
				if (_childrens[i] == child)
					_childrens = _childrens.slice(i,1);
			
			return child;
		}
		
		public function arrangeChilds():void
		{
			//must override
		}
		
		public function get display():*
		{
			return this;
		}
		
		public function getChildWithID(id:String):*
		{
			if (id == this.id)
				return this;
			else
				return null;
		}
		
		//-----------------------------------------------------------
		//
		// Handlers
		//
		//-----------------------------------------------------------

	

	}
}