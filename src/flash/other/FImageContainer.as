package flash.other
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.base.FBaseContainer;

	public class FImageContainer extends FBaseContainer
	{
		//---------------------------------------------------------------
		//
		// Variables
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Components
		//
		//---------------------------------------------------------------
		
		protected var _bitmap:Bitmap = new Bitmap();
		
		//---------------------------------------------------------------
		//
		// Construct
		//
		//---------------------------------------------------------------
		
		public function FImageContainer()
		{
			super();
			addChild(_bitmap);
		}
		
		//---------------------------------------------------------------
		//
		// Visual
		//
		//---------------------------------------------------------------
		
		override public function get contentSize():Point
		{
			if (_contentSize.equals(NULLPOINT))
				return new Point(_bitmap.width, _bitmap.height);
			else
				return super.contentSize;	
		}
		
		override public function set contentSize(value:Point):void
		{
			super._contentSize = value;
			_bitmap.width = value.x;
			_bitmap.height = value.y;
		}
		
		public function setBitmap(bitmap:Bitmap):void
		{
			_bitmap = bitmap;
			addChild(_bitmap);
			if (!_contentSize.equals(NULLPOINT))
			{
				_bitmap.width = _contentSize.x;
				_bitmap.height = _contentSize.y;
			}
		}
		
		//---------------------------------------------------------------
		//
		// Logic
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Handlers
		//
		//---------------------------------------------------------------
		
	}
}