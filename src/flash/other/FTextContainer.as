package flash.other
{
	import flash.base.FBaseContainer;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class FTextContainer extends FBaseContainer
	{
		//---------------------------------------------------------------
		//
		// Variables
		//
		//---------------------------------------------------------------
		
		protected var _textField:TextField = new TextField();
		
		//---------------------------------------------------------------
		//
		// Components
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Construct
		//
		//---------------------------------------------------------------
		
		public function FTextContainer()
		{
			super();
			addChild(_textField);
		}
		
		//---------------------------------------------------------------
		//
		// Visual
		//
		//---------------------------------------------------------------
		
		public function set text(value:String):void
		{
			_textField.text = value;
			if (_contentSize.equals(NULLPOINT))
				_textField.width = _textField.textWidth + 4;
				_textField.height = _textField.textHeight + 4;
		}
		
		public function set textFormat(tf:TextFormat):void
		{
			_textField.defaultTextFormat = tf;
			_textField.text = _textField.text;
		}
		
	
		
		//---------------------------------------------------------------
		//
		// Logic
		//
		//---------------------------------------------------------------
		
		override public function get contentSize():Point
		{
			if (_contentSize.equals(NULLPOINT))
				return new Point(_textField.width, _textField.height);
			else
				return super.contentSize;	
		}
		
		override public function set contentSize(value:Point):void
		{
			super._contentSize = value;
			_textField.width = value.x;
			_textField.height = value.y;
		}
		
		public function get textField():TextField
		{
			return _textField;
		}
		
		//---------------------------------------------------------------
		//
		// Handlers
		//
		//---------------------------------------------------------------
		
	}
}